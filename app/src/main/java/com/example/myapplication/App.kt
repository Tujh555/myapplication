package com.example.myapplication

import android.app.Application
import android.content.Context
import androidx.fragment.app.Fragment
import com.example.myapplication.di.AppComponent
import com.example.myapplication.di.DaggerAppComponent

class App : Application() {
    val component by lazy {
        DaggerAppComponent
            .builder()
            .appContext(this)
            .build()
    }
}

val Context.appComponent: AppComponent
    get() = if (this is App) {
        component
    } else {
        applicationContext.appComponent
    }

val Fragment.appComponent: AppComponent
    get() = requireContext().appComponent
package com.example.myapplication.domain

import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

interface NotesRepository {
    fun getAllNotes(): Observable<List<Note>>

    fun getNotesByUserId(uid: Int): Observable<List<Note>>

    fun addNote(note: Note): Completable

    fun deleteNote(id: Int): Completable
}

interface UserRepository {
    fun getUserById(id: Int): Single<User>

    fun addUser(user: User): Completable

    fun deleteUser(id: Int): Completable
}
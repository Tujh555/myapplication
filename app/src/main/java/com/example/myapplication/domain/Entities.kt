package com.example.myapplication.domain

data class User(
    val id: Int,
    val name: String,
    val age: Int
)

data class Note(
    val id: Int,
    val title: String,
    val content: String
)
package com.example.myapplication.di

import android.content.Context
import androidx.room.Room
import com.example.myapplication.data.NoteRepositoryImpl
import com.example.myapplication.data.UserRepositoryImpl
import com.example.myapplication.data.database.entities.AppDatabase
import com.example.myapplication.data.database.entities.NoteDao
import com.example.myapplication.data.database.entities.UserDao
import com.example.myapplication.domain.NotesRepository
import com.example.myapplication.domain.UserRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "db_name").build()
    }

    @Provides
    @Singleton
    fun provideUserDao(db: AppDatabase): UserDao = db.userDao

    @Provides
    @Singleton
    fun provideNoteDao(db: AppDatabase): NoteDao = db.noteDao
}

@Module
interface RepositoryModule {
    @Binds
    fun bindUserRepository(impl: UserRepositoryImpl): UserRepository

    @Binds
    fun bindNoteRepository(impl: NoteRepositoryImpl): NotesRepository
}
package com.example.myapplication.di

import android.content.Context
import com.example.myapplication.di.viewModels.ViewModelModule
import com.example.myapplication.presentation.mainScreen.MainFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DatabaseModule::class, RepositoryModule::class, ViewModelModule::class])
interface AppComponent {
    fun inject(fragment: MainFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun appContext(context: Context): Builder

        fun build(): AppComponent
    }
}
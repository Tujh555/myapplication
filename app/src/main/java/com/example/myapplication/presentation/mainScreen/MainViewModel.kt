package com.example.myapplication.presentation.mainScreen

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.domain.Note
import com.example.myapplication.domain.NotesRepository
import com.example.myapplication.domain.User
import com.example.myapplication.domain.UserRepository
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val noteRepository: NotesRepository
) : ViewModel() {
    private val _user = MutableLiveData<User>()
    private val _notes = MutableLiveData<List<Note>>()
    private val disposeBag = CompositeDisposable()

    val user: LiveData<User>
        get() = _user
    val notes: LiveData<List<Note>>
        get() = _notes

    init {
        userRepository
            .getUserById((1..4).random())
            .doOnSuccess { _user.postValue(it) }
            .doOnError { Log.e("MyLogs", "user not found") }
            .onErrorComplete()
            .flatMapObservable {
                noteRepository
                    .getNotesByUserId(it.id)
                    .doOnNext { note -> _notes.postValue(note) }
                    .doOnError { Log.e("MyLogs", "Notes not found") }
                    .onErrorComplete()
            }
            .subscribe()
            .also(disposeBag::add)
    }

    override fun onCleared() {
        super.onCleared()
        disposeBag.clear()
    }
}
package com.example.myapplication.data.database.entities

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

//interface NotesRepository {
//    fun getAllNotes(): Observable<List<Note>>
//
//    fun addNote(note: Note): Completable
//
//    fun deleteNote(id: Note): Completable
//}
//
//interface UserRepository {
//    fun getUserById(id: Int): Single<User>
//
//    fun addUser(user: User): Completable
//
//    fun deleteUser(id: Int): Completable
//}

@Dao
interface NoteDao {
    @Query("SELECT * FROM note ORDER BY title")
    fun getAllNotes(): Observable<List<NoteEntity>>

    @Query("SELECT * FROM note WHERE userId = :uid ORDER BY title")
    fun getNotesByUserId(uid: Int): Observable<List<NoteEntity>>

    @Query("INSERT INTO note (title, content) VALUES (:tit, :con)")
    fun insertNote(tit: String, con: String): Completable

    @Query("DELETE FROM note WHERE id = :id")
    fun deleteNote(id: Int): Completable
}

@Dao
interface UserDao {
    @Query("SELECT * FROM user WHERE id = :id")
    fun getUserById(id: Int): Single<UserEntity>

    @Query("INSERT INTO user (name, age) VALUES (:nm, :ag)")
    fun insertUser(nm: String, ag: Int): Completable

    @Query("DELETE FROM user WHERE id = :id")
    fun deleteUser(id: Int): Completable
}
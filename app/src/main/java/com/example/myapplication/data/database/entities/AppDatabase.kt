package com.example.myapplication.data.database.entities

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [NoteEntity::class, UserEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract val userDao: UserDao
    abstract val noteDao: NoteDao
}
package com.example.myapplication.data

import com.example.myapplication.data.database.entities.NoteDao
import com.example.myapplication.data.database.entities.NoteEntity
import com.example.myapplication.data.database.entities.UserDao
import com.example.myapplication.data.database.entities.UserEntity
import com.example.myapplication.domain.Note
import com.example.myapplication.domain.NotesRepository
import com.example.myapplication.domain.User
import com.example.myapplication.domain.UserRepository
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(private val userDao: UserDao) : UserRepository {
    override fun getUserById(id: Int): Single<User> {
        return userDao
            .getUserById(id)
            .subscribeOn(Schedulers.io())
            .map { it.toDomain() }
    }

    override fun addUser(user: User): Completable {
        return userDao
            .insertUser(user.name, user.age)
            .subscribeOn(Schedulers.io())
    }

    override fun deleteUser(id: Int): Completable {
        return userDao
            .deleteUser(id)
            .subscribeOn(Schedulers.io())
    }
}

class NoteRepositoryImpl @Inject constructor(private val noteDao: NoteDao) : NotesRepository {
    override fun getAllNotes(): Observable<List<Note>> {
        return noteDao
            .getAllNotes()
            .subscribeOn(Schedulers.io())
            .map {
                it.map { e -> e.toDomain() }
            }

    }

    override fun getNotesByUserId(uid: Int): Observable<List<Note>> {
        return noteDao
            .getNotesByUserId(uid)
            .subscribeOn(Schedulers.io())
            .map {
                it.map { e -> e.toDomain() }
            }
    }

    override fun addNote(note: Note): Completable {
        return noteDao
            .insertNote(note.title, note.content)
            .subscribeOn(Schedulers.io())

    }

    override fun deleteNote(id: Int): Completable {
        return noteDao
            .deleteNote(id)
            .subscribeOn(Schedulers.io())
    }

}

fun NoteEntity.toDomain() = Note(
    id = id, title = title, content = content
)

fun UserEntity.toDomain() = User(
    id = id, name = name, age = age
)